import numpy as np
import random
from datetime import datetime


def generate_instances(n_candidates, n_preferences, inst_id, incompleteness_prob=0.4, min_candidates=0.7):
    now = datetime.now()
    timestamp = now.strftime("%d-%m-%Y_%H-%M-%S")
    filename = "bruteforce_dataset/v2/inst_" + str(inst_id) + "_" + str(n_candidates) + "_" + str(
        n_preferences) + "_" + timestamp + ".election"
    open(filename, 'w+').close()
    file1 = open(filename, "a")
    arr = np.arange(n_candidates)

    for _ in range(n_preferences):
        temp_arr = np.copy(arr)
        np.random.shuffle(temp_arr)

        ncand = len(temp_arr)
        if random.uniform(0, 1) < incompleteness_prob:
            d = random.uniform(min_candidates, 1)
            ncand = int(ncand * d)
            print(ncand)
        newline = ""
        for j in range(ncand):
            if j == 0:
                newline = newline + str(temp_arr[j])
            else:
                newline = newline + ">" + str(temp_arr[j])
        newline = newline + "\n"
        print(newline)
        file1.write(newline)

    file1.close()


for i in range(10):
    for j in range(10):
        generate_instances(i + 3, ((j + 2) ** 2) * 10, 2, 0.0, 1.0)
