import itertools
import sys
from copy import deepcopy, copy
from operator import itemgetter

from tarjan import tarjan



def int_if_possible(x):
    try:
        return int(x)
    except:
        return x


def generate_graph_matrix(path):
    file = open(path, mode='r', encoding='utf-8')
    input_votes = [x.strip().split('>') for x in file.readlines()]
    file.close()

    candidates_list = sorted(list(set([x for y in input_votes for x in y])), key=lambda x: int_if_possible(x))
    candidates_index = {x: candidates_list.index(x) for x in candidates_list}
    candidates_matrix = [[0] * len(candidates_list) for _ in range(len(candidates_list))]
    for vote in input_votes:
        for i, c in enumerate(vote):
            for other_c in vote[i + 1:]:
                candidates_matrix[candidates_index[c]][candidates_index[other_c]] += 1
    # print(candidates_list)
    # print(candidates_matrix)
    return candidates_matrix, candidates_list


def crete_majority_matrix(matrix):
    new_matrix = deepcopy(matrix)
    for i in range(len(new_matrix)):
        for j in range(len(new_matrix[i])):
            if new_matrix[i][j] > new_matrix[j][i]:
                new_matrix[i][j] = new_matrix[i][j] - new_matrix[j][i]
                new_matrix[j][i] = 0
            elif new_matrix[i][j] < new_matrix[j][i]:
                new_matrix[j][i] = new_matrix[j][i] - new_matrix[i][j]
                new_matrix[i][j] = 0
            else:
                new_matrix[j][i] = 0
                new_matrix[i][j] = 0

    return new_matrix


def get_disagreements_for_stack(stack, majority_matrix):
    s = 0
    for e in stack:
        s += majority_matrix[e[1]][e[0]]
    return s


def get_disagreement_for_edge(edge, majority_matrix):
    return majority_matrix[edge[1]][edge[0]]


def create_stack_from_matrix(matrix):
    stack = set()
    for i in range(len(matrix)):
        for j in range(len(matrix)):
            if matrix[i][j] > 0:
                stack.add((i, j))
    return stack


def transitive_closure(matrix):
    tc = [[0] * len(matrix) for _ in range(len(matrix))]

    def bf(s, v):
        nonlocal tc
        for i in range(len(tc)):
            if matrix[v][i] > 0 and tc[s][i] == 0:
                tc[s][i] = 1
                bf(s, i)

    for i in range(len(tc)):
        bf(i, i)
    return tc


def add_transitive_closure_to_stack(stack, size):
    new_matrix = [[0] * size for _ in range(size)]
    for e in stack:
        new_matrix[e[0]][e[1]] = 1
    new_matrix = transitive_closure(new_matrix)
    return create_stack_from_matrix(new_matrix)


def get_all_simple_cycles_from_matrix(matrix):
    import networkx as nx

    # Create Directed Graph
    G = nx.DiGraph()

    # Add a list of nodes:
    G.add_nodes_from(list(range(len(matrix))))

    # Add a list of edges:
    G.add_edges_from(create_stack_from_matrix(matrix))

    # Return a list of cycles described as a list o nodes
    return sorted(list(nx.simple_cycles(G)), key=len, reverse=False)


def get_lower_bound(majority_mat):
    matrix = deepcopy(majority_mat)
    cycles = get_all_simple_cycles_from_matrix(matrix)
    removed = 0
    while len(cycles) > 0:
        cycle = cycles[0]
        m = matrix[cycle[-1]][cycle[0]]
        for i, x in enumerate(cycle[:-1]):
            m = min(matrix[x][cycle[i + 1]], m)
        matrix[cycle[-1]][cycle[-1]] -= m
        for i, x in enumerate(cycle[:-1]):
            matrix[x][cycle[i + 1]] -= m
        removed += m
        cycles = get_all_simple_cycles_from_matrix(matrix)
    return removed


def tarjan_from_stack(stack):
    tj_input = {}
    for e in stack:
        if e[0] in tj_input:
            tj_input[e[0]].append(e[1])
        else:
            tj_input[e[0]] = [e[1]]
    return list(reversed(tarjan(tj_input)))


def create_stack_from_components(components, stack, majority_matrix):
    unusable = set()
    for i, first_part in enumerate(components[:-1]):
        for second_part in components[i + 1:]:
            for v1 in first_part:
                for v2 in second_part:
                    if (v1, v2) not in stack and majority_matrix[v1][v2] > 0:
                        if (v2, v1) not in stack:
                            stack.add((v1, v2))
                            stack = add_transitive_closure_to_stack(stack, len(majority_matrix))
                        else:
                            unusable.add((v1, v2))
    return stack, unusable


def score_indexes_negative(graph_matrix, vote):
    score = 0
    visited = []
    for v in reversed(vote):
        visited.append(v)
        for vis in visited:
            score += graph_matrix[vis][v]
    return score


def subtract_stack_from_stack_undirected(main, stack):
    r = set()
    for i in main:
        if i not in stack and (i[1], i[0]) not in stack:
            r.add(i)
    return r


def get_hash_of_stack(s):
    # set is immutable, so we need to get hash of string representation of the set.
    # To preserve the hash for same sets, we need to sort the elements once it is a list
    l = list(s)
    l.sort(key=itemgetter(0, 1))
    return hash(str(l))


def kemeny_bab_with_stack(graph_matrix, candidates_list):
    majority_matrix = crete_majority_matrix(graph_matrix)
    # pprint(majority_matrix)
    best_solution = []
    visited_stacks = set()
    best_score = sys.maxsize
    best_lower_bound = get_lower_bound(majority_matrix)
    matrix_size = len(majority_matrix)
    all_edges_as_stack = create_stack_from_matrix(majority_matrix)
    # print('stack size', len(all_edges_as_stack))
    total_checked = 0

    def rec(stack, r):
        nonlocal total_checked
        nonlocal best_score
        nonlocal best_solution
        nonlocal best_lower_bound
        nonlocal visited_stacks
        total_checked += 1
        hash_of_stack = get_hash_of_stack(stack)
        if hash_of_stack in visited_stacks:
            return
        stack = add_transitive_closure_to_stack(stack, matrix_size)
        hash_of_stack = get_hash_of_stack(stack)
        if hash_of_stack in visited_stacks:
            return
        visited_stacks.add(hash_of_stack)
        # if len(visited_stacks) % 1000 == 0:
        # print(r, len(visited_stacks), total_checked)
        # print(stack)

        lower_bound_score = get_disagreements_for_stack(stack, majority_matrix)
        if lower_bound_score > best_lower_bound:
            return
        decomposition = tarjan_from_stack(subtract_stack_from_stack_undirected(all_edges_as_stack, stack))
        stack, unusable = create_stack_from_components(decomposition, stack, majority_matrix)
        # print(decomposition, r)
        # if decomposition of the graph returns no cycles
        if sum([len(x) for x in decomposition]) == len(decomposition):
            t = tarjan_from_stack(stack)
            vote = [x[0] for x in t]
            score = score_indexes_negative(graph_matrix, vote)
            if score < best_score:
                best_score = score
                best_solution = vote
                # print(score)
                # print(vote)
                # print(stack)
                if lower_bound_score < best_lower_bound:
                    best_lower_bound = lower_bound_score
                    # print('best_lower_bound', best_lower_bound)
        possible_edges = subtract_stack_from_stack_undirected(all_edges_as_stack, stack)
        for e in sorted(list(possible_edges), key=lambda x: majority_matrix[x[0]][x[1]], reverse=True):
            # print(e, len(possible_edges), r)
            rec(stack | {e}, r + 1)

    rec(set(), 0)
    print('total_checked', total_checked)
    return best_score, best_solution, [candidates_list[x] for x in best_solution]


def solve(file_path):
    import time
    start = time.time()
    candidates_matrix, candidates_list = generate_graph_matrix(file_path)
    best_score, best_vote_index, best_vote = kemeny_bab_with_stack(candidates_matrix, candidates_list)
    end = time.time()
    print('time to compute: ', end - start)
    return best_vote, best_score


def solve_bruteforce(file_path):
    candidates_matrix, candidates_list = generate_graph_matrix(file_path)
    best_score, best_vote_index, best_vote = kemeny_brute_force_negative(candidates_matrix, candidates_list)
    return best_vote, best_score, best_vote_index


def kemeny_brute_force_negative(graph_matrix, candidates_list):
    if len(candidates_list) == 1:
        return [0], 0, candidates_list
    all_combinations = itertools.permutations(range(len(graph_matrix)), len(graph_matrix))
    best_score = 9999999999999999999
    best_vote = None
    for vote in all_combinations:
        s = score_indexes_negative(graph_matrix, vote)
        if s < best_score:
            best_score = s
            best_vote = vote
    return best_score, best_vote, [candidates_list[x] for x in best_vote]


def get_disagreements_for_partial_sort(partial_sort, majority_matrix):
    if len(partial_sort) == 1:
        return 0
    s = 0
    for i, v_up in enumerate(partial_sort):
        for v_low in partial_sort[i + 1:]:
            for x in v_up:
                for y in v_low:
                    s += majority_matrix[y][x]
    return s


def get_stack_from_component(order, majority_matrix):
    stack = set()
    for i in order:
        for j in order:
            if i != j and majority_matrix[i][j] > 0:
                stack.add((i, j))
    return stack


def kemeny_not_davenport_bab(graph_matrix, candidates_list):
    majority_matrix = crete_majority_matrix(graph_matrix)
    best_lower_bound = get_lower_bound(majority_matrix)
    total_checked = 0

    def rec(component, inherited_lb_score, r, last):
        nonlocal total_checked
        nonlocal best_lower_bound
        total_checked += 1
        if inherited_lb_score > best_lower_bound:
            return sys.maxsize, component
        decomposition = tarjan_from_stack(get_stack_from_component(component, majority_matrix))
        # if decomposition of the graph returns no cycles
        if sum([len(x) for x in decomposition]) == len(decomposition):
            vote = [x[0] for x in decomposition]
            score = score_indexes_negative(graph_matrix, vote)
            inherited_copy = copy(inherited_lb_score)
            inherited_lb_score += get_disagreements_for_partial_sort(decomposition, majority_matrix)
            if inherited_lb_score < best_lower_bound and last:
                best_lower_bound = inherited_lb_score
            return score, vote
        inherited_lb_score += get_disagreements_for_partial_sort(decomposition, majority_matrix)
        num_to_expand = sum([1 for x in decomposition if len(x) > 1])
        for comp_i, comp in enumerate(decomposition):
            if len(comp) > 1:
                best_in_comp = sys.maxsize - 1
                best_in_comp_vote = []
                for i in comp:
                    rest = [c for c in comp if c != i]
                    added_score = get_disagreements_for_partial_sort([[i], rest], majority_matrix)
                    s_r, v_r = rec(rest, inherited_lb_score + added_score, r + 1, last and comp_i == num_to_expand)
                    v = [i] + v_r
                    s = score_indexes_negative(graph_matrix, v)
                    if s < best_in_comp:
                        best_in_comp = s
                        best_in_comp_vote = v
                inherited_lb_score += get_disagreements_for_partial_sort([[x] for x in best_in_comp_vote],
                                                                         majority_matrix)
                decomposition[comp_i] = best_in_comp_vote
        fnal_vote = [j for sub in decomposition for j in sub]
        return score_indexes_negative(graph_matrix, fnal_vote), fnal_vote

    best_score, best_solution = rec(list(range(len(majority_matrix))), 0, 0, True)
    print('total_checked', total_checked)
    return best_score, best_solution, [candidates_list[x] for x in best_solution]


def solve_non_davenport(file_path):
    import time
    start = time.time()
    candidates_matrix, candidates_list = generate_graph_matrix(file_path)
    best_score, best_vote_index, best_vote = kemeny_not_davenport_bab(candidates_matrix, candidates_list)
    end = time.time()
    print('time to compute: ', end - start)
    return best_vote, best_score, best_vote_index


if __name__ == '__main__':
    print(solve_non_davenport('data/bruteforce_dataset/v2/inst_2_6_640_28-12-2020_10-44-38.election'))
    print(solve_bruteforce('data/bruteforce_dataset/v2/inst_2_6_640_28-12-2020_10-44-38.election'))
    print(solve('data/bruteforce_dataset/v2/inst_2_6_640_28-12-2020_10-44-38.election'))
    # custom_matrix = [[0, 0, 1, 0, 1],
    #                  [1, 0, 0, 1, 0],
    #                  [0, 1, 0, 0, 1],
    #                  [1, 0, 1, 0, 0],
    #                  [0, 1, 0, 1, 0]]
    # print(kemeny_not_davenport_bab(custom_matrix, [0, 1, 2, 3, 4]))
    # print(kemeny_bab_with_stack(custom_matrix, [0, 1, 2, 3, 4]))
