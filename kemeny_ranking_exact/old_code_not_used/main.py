import time

from old_code_not_used.graph_generation import generate_graph_matrix, crete_majority_matrix, create_graph_from_matrix
from old_code_not_used.kemeny import kemeny_brute_force_negative, \
    kemeny_bab_with_stack

if __name__ == '__main__':
    candidates_matrix, candidates_list = generate_graph_matrix(
        'data/bruteforce_dataset/inst_1_8_10_23-11-2020_10-19-28.election')
    majority_matrix = crete_majority_matrix(candidates_matrix)
    majority_graph = create_graph_from_matrix(majority_matrix, candidates_list, True, 'majority.png')
    whole_graph = create_graph_from_matrix(candidates_matrix, candidates_list, True, 'graph.png')
    start1 = time.time()
    best_score, best_vote_index, best_vote = kemeny_bab_with_stack(candidates_matrix, candidates_list)
    print(best_score, best_vote_index, best_vote)
    end1 = time.time()
    print('time to finish 1', end1 - start1)
    print()
    start2 = time.time()
    best_score, best_vote_index, best_vote = kemeny_brute_force_negative(candidates_matrix, candidates_list)
    print(best_score, best_vote_index, best_vote)
    end2 = time.time()

    print('time to finish 2', end2 - start2)
    print()

    #
    # small_cycle_matrix = create_smaller_matrix_from_matrix(candidates_matrix, [0, 4, 3, 5])
    # small_cycle_names = [x for i, x in enumerate(candidates_list) if i in [0, 4, 3, 5]]
    #
    # best_score, best_vote_index, best_vote = kemeny_brute_force_negative(small_cycle_matrix, small_cycle_names)
    # print(best_score, best_vote_index, best_vote)
    # print()
    #
    # best_score, best_vote_index, best_vote = kemeny_dfs_with_stack(small_cycle_matrix, small_cycle_names)
    # print(best_score, best_vote_index, best_vote)
    #
    # small_cycle_majority_matrix = crete_majority_matrix(small_cycle_matrix)
    # create_graph_from_matrix(small_cycle_majority_matrix, [0,1,2,3], True, 'majority_small.png')
    # create_graph_from_matrix(small_cycle_matrix, [0,1,2,3], True, 'graphy_small.png')
