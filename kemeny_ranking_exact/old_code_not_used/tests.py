import sys
from copy import deepcopy

from old_code_not_used.graph_generation import add_connections_between_cycles_to_matrix, decompose_graph_from_matrix, \
    create_graph_from_matrix, topological_sort, number_of_nonzero_edges, get_largest_weight_edge_indexes, is_acyclic
from old_code_not_used.kemeny import score_indexes_negative


def test_add_connections():
    matrix_a = [[0, 2, 3, 4],
                [5, 0, 6, 7],
                [8, 9, 0, 10],
                [11, 12, 13, 0], ]
    matrix_b = [[0, 0, 0, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0], ]

    print(add_connections_between_cycles_to_matrix(matrix_b, matrix_a, [[0], [1, 2], [3]]))


def test_rec():
    graph_matrix = [[0, 102, 107, 101, 112, 101, 97, 94], [98, 0, 108, 97, 114, 103, 96, 96],
                    [93, 92, 0, 94, 101, 99, 84, 87], [99, 103, 106, 0, 107, 96, 88, 91],
                    [88, 86, 99, 93, 0, 96, 88, 79], [99, 97, 101, 104, 104, 0, 103, 76],
                    [103, 104, 116, 112, 112, 97, 0, 94], [106, 104, 113, 109, 121, 124, 106, 0]]

    solution = [[0, 0, 14, 0, 24, 0, 0, 0], [0, 0, 16, 0, 28, 6, 0, 0], [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 12, 0, 14, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 2, 8, 8, 0, 0, 0], [0, 8, 32, 24, 24, 0, 0, 0],
                [12, 8, 26, 18, 42, 48, 12, 0]]
    majority = [
        [0, 4, 0, 2, 0, 2, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 6, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 6, 0], [6, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0]]
    removed_s = 46
    path = [(6, 3), (5, 3), (6, 1), (1, 5)]
    r = 0
    past_decompositions = [[[7], [0, 1, 3, 5, 6], [2], [4]], [[0, 1, 3, 5, 6], [2], [4], [7]],
                           [[0, 1, 3, 5, 6], [2], [4], [7]], [
                               [0, 1, 3, 5, 6], [2], [4], [7]]]
    best_solution = []
    best_score = sys.maxsize
    best_removal = sys.maxsize

    def rec(solution_o, majority_o, removed_s, path, r, past_decompositions):
        print('r', r)
        nonlocal best_score
        nonlocal best_solution
        nonlocal best_removal
        # if removed_s > best_removal:
        #     return
        # print(path)
        solution = deepcopy(solution_o)
        majority = deepcopy(majority_o)
        decomposition = decompose_graph_from_matrix(majority)
        solution, majority = add_connections_between_cycles_to_matrix(solution, majority, decomposition)
        print('decomposition', decomposition)
        if len(decomposition) == len(majority):
            vote = topological_sort(solution)
            # print('decomposition', decomposition)
            # print(vote)
            score = score_indexes_negative(graph_matrix, vote)
            print('score', score)
            # print(score)
            # print(removed_s)

            if score <= best_score:
                print('beating best', score, vote)
                print(past_decompositions)
                print('last decomposition', decomposition)
                print('last solution', solution)
                create_graph_from_matrix(solution, list(range(len(solution))), plot=True, plot_name='last_solution.png')
                # print(solution)
                best_score = score
                best_solution = vote
                best_removal = removed_s
            return
        tried = []
        non_zero_edges = number_of_nonzero_edges(majority)
        i = 0
        while len(tried) < non_zero_edges:
            print('---------------------')
            print('r in while', r)

            i += 1
            x, y, s = get_largest_weight_edge_indexes(majority, black_list=tried)
            print(x, y, s, )
            for q in majority:
                print(q)
            print('non_zero_edges', non_zero_edges)
            print('i', i)
            tried.append((x, y))
            solution[x][y] = s
            majority[x][y] = 0
            if is_acyclic(solution):
                rec(solution, majority, removed_s + s, path + [(x, y)], r + 1, past_decompositions + [decomposition])
            else:
                print(x,y, 'would make it cyclic')
            majority[x][y] = s
            solution[x][y] = 0

    rec(solution, majority, removed_s, path, r, past_decompositions)


test_rec()
