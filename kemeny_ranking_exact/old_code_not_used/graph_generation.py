import functools
import sys
from copy import deepcopy

import igraph as ig
import numpy as np
from tarjan import tarjan


def int_if_possible(x):
    try:
        return int(x)
    except:
        return x


def generate_graph_matrix(path):
    file = open(path, mode='r', encoding='utf-8')
    input_votes = [x.strip().split('>') for x in file.readlines()]
    file.close()

    candidates_list = sorted(list(set([x for y in input_votes for x in y])), key=lambda x: int_if_possible(x))
    candidates_index = {x: candidates_list.index(x) for x in candidates_list}
    candidates_matrix = [[0] * len(candidates_list) for _ in range(len(candidates_list))]
    for vote in input_votes:
        for i, c in enumerate(vote):
            for other_c in vote[i + 1:]:
                candidates_matrix[candidates_index[c]][candidates_index[other_c]] += 1
    # print(candidates_list)
    # print(candidates_matrix)
    return candidates_matrix, candidates_list


def create_graph_from_matrix(matrix, labels, plot=False, plot_name='graph.png'):
    graph = ig.Graph(directed=True)
    for i in labels:
        graph.add_vertex(i, label=i)
    # graph.add_vertices(candidates_list)
    for c_from, c in enumerate(matrix):
        for c_to, value in enumerate(c):
            if value > 0:
                graph.add_edge(source=c_from, target=c_to, capacity=value, label=value)
    if plot:
        layout = graph.layout("circle")
        ig.plot(graph, layout=layout, target=plot_name)
    return graph


def create_matrix_from_graph(graph: ig.Graph):
    return graph.get_adjacency(attribute='capacity').data, [x['name'] for x in graph.vs]


def crete_majority_matrix(matrix):
    new_matrix = deepcopy(matrix)
    for i in range(len(new_matrix)):
        for j in range(len(new_matrix[i])):
            if new_matrix[i][j] > new_matrix[j][i]:
                new_matrix[i][j] = new_matrix[i][j] - new_matrix[j][i]
                new_matrix[j][i] = 0
            elif new_matrix[i][j] < new_matrix[j][i]:
                new_matrix[j][i] = new_matrix[j][i] - new_matrix[i][j]
                new_matrix[i][j] = 0
            else:
                new_matrix[j][i] = 0
                new_matrix[i][j] = 0

    return new_matrix


def get_max_flow_in_3_cycles(graph: ig.Graph):
    paths = []
    for v in graph.vs:
        pa = graph.get_all_simple_paths(v, cutoff=2)
        print(pa)
        for p in pa:
            if len(p) == 3 and len(list(graph.es.select(_source=p[-1], _target=p[0]))) > 0:
                paths.append(p + [p[0]])
    print('paths', paths)
    min_flow = 0
    for path in paths:
        min_flow += min([graph.es.select(_source=path[i], _target=path[i + 1])[0]['capacity'] for i in range(3)])
    print(min_flow)


def decompose_graph(graph: ig.Graph):
    def compare(graph1, graph2):
        s = 0
        for i in graph1.vs:
            for j in graph2.vs:
                edge = graph.es.select(_source=i, _target=j)
                if len(edge) == 1:
                    s += edge[0]['capacity']
                else:

                    edge = graph.es.select(_source=j, _target=i)
                    if len(edge) == 1:
                        s -= edge[0]['capacity']

        if s < 0:
            return -1
        elif s > 0:
            return 1
        else:
            return 0

    parts = list(graph.decompose())
    parts.sort(key=functools.cmp_to_key(compare))
    print('decomposed into ', len(parts), [len(x.vs) for x in parts])
    return parts


def populate_graph_with_edges_from_other_graph(graph_small: ig.Graph, graph_big: ig.Graph):
    return graph_big.subgraph([x['name'] for x in graph_small.vs])


def transitive_closure(matrix):
    tc = [[0] * len(matrix) for _ in range(len(matrix))]

    def bf(s, v):
        nonlocal tc
        for i in range(len(tc)):
            if matrix[v][i] > 0 and tc[s][i] == 0:
                tc[s][i] = 1
                bf(s, i)

    for i in range(len(tc)):
        bf(i, i)
    return tc


def subgraph_from_matrix(matrix, vertices_indexes):
    new_matrix = [[0] * len(matrix) for _ in range(len(matrix))]
    for i in range(len(matrix)):
        for j in range(len(matrix)):
            if i in vertices_indexes and j in vertices_indexes:
                new_matrix[i][j] = matrix[i][j]
    return new_matrix


def decompose_graph_from_matrix(matrix, transitive_closure_matrix=None):
    def compare(component1, component2):
        s = 0
        for i in component1:
            for j in component2:
                s += matrix[i][j]
                s -= matrix[j][i]

        if s < 0:
            return 1
        elif s > 0:
            return -1
        else:
            return 0

    components = []
    if transitive_closure_matrix is not None:
        tc = transitive_closure_matrix
    else:
        tc = transitive_closure(matrix)
    for i in range(len(matrix)):
        if tc[i][i] == 0:
            components.append([i])
        else:
            found = None
            for j in components:
                if tc[j[0]][i] > 0 and tc[i][j[0]] > 0:
                    j.append(i)
                    found = True
                    break
            if found is None:
                components.append([i])
    components.sort(key=functools.cmp_to_key(compare))
    return components  # [subgraph_from_matrix(matrix, x) for x in components],


def get_largest_weight_edge_indexes(matrix, vertice=None, black_list=None):
    m = 0
    m_from = None
    m_to = None
    for i in range(len(matrix)) if vertice is None else [vertice]:
        for j in range(len(matrix)):
            if matrix[i][j] > m and (i, j) not in black_list:
                m = matrix[i][j]
                m_from = i
                m_to = j
    return m_from, m_to, m


def is_acyclic(graph_matrix=None, transitive_closure_matrix=None):
    if transitive_closure_matrix is not None:
        tc = transitive_closure_matrix
    else:
        tc = transitive_closure(graph_matrix)
    for i in range(len(tc)):
        if tc[i][i] == 1:
            return False
    return True


def add_connections_between_cycles_to_matrix(solution_matrix, majority_matrix, components_indexes):
    if len(components_indexes) == 1:
        return solution_matrix
    for i, first_part in enumerate(components_indexes[:-1]):
        for second_part in components_indexes[i + 1:]:
            for v1 in first_part:
                for v2 in second_part:
                    if solution_matrix[v1][v2] == 0 and solution_matrix[v2][v1] == 0:
                        solution_matrix[v1][v2] = majority_matrix[v1][v2]
                        majority_matrix[v1][v2] = 0
    return solution_matrix, majority_matrix


def topological_sort(matrix):
    def compare(i, j):

        if matrix[i][j] > matrix[j][i]:
            return -1
        elif matrix[i][j] < matrix[j][i]:
            return 1
        else:
            return 0

    l = list(range(len(matrix)))
    l.sort(key=functools.cmp_to_key(compare))
    return l


def number_of_nonzero_edges(matrix):
    return sum(y > 0 for x in matrix for y in x)


def create_matrix_from_stack(stack: list, referential_matrix):
    new_matrix = [[0] * len(referential_matrix) for _ in range(len(referential_matrix))]
    for e in stack:
        new_matrix[e[0]][e[1]] = referential_matrix[e[0]][e[1]]
    return new_matrix


def create_stack_from_components(components, stack, majority_matrix):
    unusable = set()
    for i, first_part in enumerate(components[:-1]):
        for second_part in components[i + 1:]:
            for v1 in first_part:
                for v2 in second_part:
                    if (v1, v2) not in stack and majority_matrix[v1][v2] > 0:
                        if (v2, v1) not in stack:
                            stack.add((v1, v2))
                            stack = add_transitive_closure_to_stack(stack, len(majority_matrix))
                        else:
                            unusable.add((v1, v2))
    return stack, unusable


def create_stack_from_matrix(matrix):
    stack = set()
    for i in range(len(matrix)):
        for j in range(len(matrix)):
            if matrix[i][j] > 0:
                stack.add((i, j))
    return stack


def remove_stack_from_matrix(stack, matrix, in_place=True):
    if not in_place:
        matrix = deepcopy(matrix)
    for e in stack:
        matrix[e[0]][e[1]] = 0
    return matrix


def tarjan_from_stack(stack):
    tj_input = {}
    for e in stack:
        if e[0] in tj_input:
            tj_input[e[0]].append(e[1])
        else:
            tj_input[e[0]] = [e[1]]
    return list(reversed(tarjan(tj_input)))


def create_smaller_matrix_from_matrix(matrix, indexes):
    new_matrix = [[0] * len(indexes) for _ in range(len(indexes))]
    for i, old_i in enumerate(indexes):
        for j, old_j in enumerate(indexes):
            new_matrix[i][j] = matrix[old_i][old_j]
    return new_matrix


def get_score_for_removed_stack(stack, matrix):
    score = 0
    for e in stack:
        score += matrix[e[0]][e[1]]
    return score


def get_all_simple_cycles_from_matrix(matrix):
    import networkx as nx

    # Create Directed Graph
    G = nx.DiGraph()

    # Add a list of nodes:
    G.add_nodes_from(list(range(len(matrix))))

    # Add a list of edges:
    G.add_edges_from(create_stack_from_matrix(matrix))

    # Return a list of cycles described as a list o nodes
    return nx.simple_cycles(G)


def get_lower_bound(majority_mat):
    matrix = deepcopy(majority_mat)
    cycles = get_all_simple_cycles_from_matrix(matrix)
    removed = 0
    while cycles:
        try:
            cycle = next(cycles)
        except:
            print(removed)
            return removed
        m = matrix[cycle[-1]][cycle[0]]
        for i, x in enumerate(cycle[:-1]):
            m = min(matrix[x][cycle[i + 1]], m)
        matrix[cycle[-1]][cycle[-1]] -= m
        for i, x in enumerate(cycle[:-1]):
            matrix[x][cycle[i + 1]] -= m
        removed += m
        cycles = get_all_simple_cycles_from_matrix(matrix)


def get_disagreements_for_stack(stack, majority_matrix):
    new_matrix = [[0] * len(majority_matrix) for _ in range(len(majority_matrix))]
    for e in stack:
        new_matrix[e[0]][e[1]] = 1
    s = 0
    for i in range(len(new_matrix)):
        for j in range(len(new_matrix)):
            if new_matrix[i][j] > 0 and majority_matrix[j][i] > 0:
                s += majority_matrix[j][i]
    return s


def subtract_stack_from_stack_undirected(main, stack):
    r = set()
    for i in main:
        if i not in stack and (i[1], i[0]) not in stack:
            r.add(i)
    return r


def is_stack_cycle_free(stack):
    t = tarjan_from_stack(stack)
    return len(t) == sum([len(x) for x in t])


def add_transitive_closure_to_stack(stack, size):
    new_matrix = [[0] * size for _ in range(size)]
    for e in stack:
        new_matrix[e[0]][e[1]] = 1
    new_matrix = transitive_closure(new_matrix)
    return create_stack_from_matrix(new_matrix)
