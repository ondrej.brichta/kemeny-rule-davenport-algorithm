import itertools
import sys
from copy import deepcopy, copy

from old_code_not_used.graph_generation import crete_majority_matrix, is_acyclic, decompose_graph_from_matrix, \
    add_connections_between_cycles_to_matrix, get_largest_weight_edge_indexes, number_of_nonzero_edges, \
    topological_sort, create_graph_from_matrix, create_stack_from_components, \
    create_stack_from_matrix, tarjan_from_stack, get_disagreements_for_stack, get_lower_bound, \
    add_transitive_closure_to_stack, subtract_stack_from_stack_undirected


def kemeny_brute_force(graph_matrix, candidates_list):
    if len(candidates_list) == 1:
        return [0], 0, candidates_list
    all_combinations = itertools.permutations(range(len(graph_matrix)), len(graph_matrix))
    best_score = 0
    best_vote = None
    for vote in all_combinations:
        s = score_indexes(graph_matrix, vote)
        if s > best_score:
            best_score = s
            best_vote = vote
    return best_vote, best_score, [candidates_list[x] for x in best_vote]


def kemeny_brute_force_negative(graph_matrix, candidates_list):
    if len(candidates_list) == 1:
        return [0], 0, candidates_list
    all_combinations = itertools.permutations(range(len(graph_matrix)), len(graph_matrix))
    best_score = 9999999999999999999
    best_vote = None
    for vote in all_combinations:
        s = score_indexes_negative(graph_matrix, vote)
        if s < best_score:
            best_score = s
            best_vote = vote
    return best_vote, best_score, [candidates_list[x] for x in best_vote]


def score_indexes(graph_matrix, vote):
    score = 0
    visited = []
    for v in reversed(vote):
        visited.append(v)
        for vis in visited:
            score += graph_matrix[v][vis]
    return score


def score_indexes_negative(graph_matrix, vote):
    score = 0
    visited = []
    for v in reversed(vote):
        visited.append(v)
        for vis in visited:
            score += graph_matrix[vis][v]
    return score


def delete_from_matrix(matrix, index):
    m = deepcopy(matrix)
    for i in m:
        i.pop(index)
    m.pop(index)
    return m


def condorcet_rule(graph_matrix: list, candidates_list: list):
    removed = []
    i = 0
    while i < len(candidates_list):
        if sum([graph_matrix[x][i] for x in range(len(candidates_list))]) == 0:
            print('deleting ', i, candidates_list[i])
            graph_matrix = delete_from_matrix(graph_matrix, i)
            removed.append(candidates_list.pop(i))
            i = 0
            if len(candidates_list) == 1:
                removed.append(candidates_list.pop(0))
                graph_matrix = [[]]
            continue
        i += 1
    return removed, graph_matrix, candidates_list


def extended_condorcet_rule(graph_matrix: list, candidates_list: list):
    removed = []
    for i in reversed(range(len(candidates_list))):
        if i == len(candidates_list):
            break
        if sum([graph_matrix[x][i] for x in range(len(candidates_list))]) == 0:
            graph_matrix = delete_from_matrix(graph_matrix, i)
            removed = candidates_list.pop(i)
    return removed, graph_matrix, candidates_list


def kemeny_dfs(graph_matrix, candidates_list):
    solution_matrix = [[0] * len(graph_matrix) for _ in range(len(graph_matrix))]
    majority_matrix = crete_majority_matrix(graph_matrix)
    best_solution = []
    best_score = sys.maxsize
    best_removal = sys.maxsize
    print(majority_matrix)
    print(graph_matrix)
    qq = 0

    def rec(solution_o, majority_o, removed_s, path, r, past_decompositions):
        nonlocal qq
        # create_graph_from_matrix(majority_o, list(range(len(majority_o))), plot=True,
        #                          plot_name=f'images/majority/{"{:0>4d}".format(qq)}_majority.png')
        # create_graph_from_matrix(solution_o, list(range(len(solution_o))), plot=True,
        #                          plot_name=f'images/solution/{"{:0>4d}".format(qq)}_solution.png')
        qq += 1
        nonlocal best_score
        nonlocal best_solution
        nonlocal best_removal
        # if removed_s > best_removal:
        #     return
        # print(path)
        solution = deepcopy(solution_o)
        majority = deepcopy(majority_o)

        decomposition = decompose_graph_from_matrix(majority)
        solution, majority = add_connections_between_cycles_to_matrix(solution, majority, decomposition)
        # print('decomposition', decomposition)
        # if decomposition == [[0, 5, 6], [1], [2], [3], [4], [7]]:
        #     create_graph_from_matrix(majority, list(range(len(solution))), plot=True, plot_name='near_last_solution.png')
        #     print(solution_o, majority_o,removed_s, path, r, past_decompositions )
        #     exit()
        if (6, 0) in path:
            print('here I am')
        if len(decomposition) == len(majority):
            vote = topological_sort(solution)
            # print('decomposition', decomposition)
            # print(vote)
            score = score_indexes_negative(graph_matrix, vote)
            # print(score)
            # print(removed_s)

            if score < best_score:
                print('beating best', score, vote)
                print(past_decompositions)
                print('last decomposition', decomposition)
                print('last solution', solution)
                create_graph_from_matrix(solution, list(range(len(solution))), plot=True, plot_name='last_solution.png')
                # print(solution)
                best_score = score
                best_solution = vote
                best_removal = removed_s
            return

        tried = []
        non_zero_edges = number_of_nonzero_edges(majority)
        i = 0
        while len(tried) < non_zero_edges:
            i += 1
            x, y, s = get_largest_weight_edge_indexes(majority, black_list=tried)
            tried.append((x, y))
            solution[x][y] = s
            majority[x][y] = 0
            if is_acyclic(solution):
                rec(solution, majority, removed_s + s, path + [(x, y)], r + 1, past_decompositions + [decomposition])
            solution[x][y] = 0
            majority[x][y] = s

    rec(deepcopy(solution_matrix), deepcopy(majority_matrix), 0, [], 0, [])
    return best_score, best_solution, [candidates_list[x] for x in best_solution]


def kemeny_dfs_with_stack(graph_matrix, candidates_list):
    majority_matrix = crete_majority_matrix(graph_matrix)
    best_solution = []
    best_score = sys.maxsize
    matrix_size = len(majority_matrix)
    all_edges_as_stack = create_stack_from_matrix(majority_matrix)

    def rec(stack, removed_stack, r):
        nonlocal best_score
        nonlocal best_solution
        decomposition = tarjan_from_stack(all_edges_as_stack - stack - removed_stack)
        stack = create_stack_from_components(decomposition, stack, majority_matrix)
        # vote = topological_sort(create_matrix_from_stack(stack, majority_matrix))
        vote2 = [x[0] for x in tarjan_from_stack(stack)]
        score = score_indexes_negative(graph_matrix, vote2)
        if score < best_score:
            best_score = score
            best_solution = vote2
        possible_edges = all_edges_as_stack - stack - removed_stack
        for e in possible_edges:
            rec(copy(stack), removed_stack | {e}, r + 1)

    rec(set(), set(), 0)
    return best_score, best_solution, [candidates_list[x] for x in best_solution]


def kemeny_bab_with_stack_and_removing(graph_matrix, candidates_list):
    majority_matrix = crete_majority_matrix(graph_matrix)
    best_solution = []
    best_score = sys.maxsize
    best_lower_bound = get_lower_bound(majority_matrix)
    matrix_size = len(majority_matrix)
    all_edges_as_stack = create_stack_from_matrix(majority_matrix)

    def rec(stack, removed_stack, r):
        nonlocal best_score
        nonlocal best_solution
        nonlocal best_lower_bound
        # lower_bound_score = get_score_for_removed_stack(removed_stack, majority_matrix)
        lower_bound_score = get_disagreements_for_stack(stack, majority_matrix)
        if lower_bound_score > best_lower_bound:
            return
        decomposition = tarjan_from_stack(
            subtract_stack_from_stack_undirected(all_edges_as_stack, stack) - removed_stack)
        stack, unsuable = create_stack_from_components(decomposition, stack, majority_matrix)
        removed_stack = removed_stack | unsuable
        possible_edges = all_edges_as_stack - stack - removed_stack
        # print(stack)
        # print(removed_stack)
        # print()
        # print('decomposition', decomposition)
        # print(lower_bound_score, r, len(possible_edges), removed_stack)
        if sum([len(x) for x in decomposition]) == len(decomposition):
            vote2 = [x[0] for x in tarjan_from_stack(stack)]
            score = score_indexes_negative(graph_matrix, vote2)
            if score < best_score and len(possible_edges) == 0:
                best_score = score
                best_solution = vote2
                if len(possible_edges) == 0 and lower_bound_score < best_lower_bound:
                    best_lower_bound = lower_bound_score
                    print('best_lower_bound', best_lower_bound)
                    print(score)
                    print(vote2)
        for e in sorted(list(possible_edges), key=lambda x: majority_matrix[x[0]][x[1]], reverse=False):
            # print('adding to removed', e)
            rec(copy(stack), removed_stack | {e}, r + 1)

    rec(set(), set(), 0)
    return best_score, best_solution, [candidates_list[x] for x in best_solution]


def kemeny_bab_with_stack(graph_matrix, candidates_list):
    majority_matrix = crete_majority_matrix(graph_matrix)
    best_solution = []
    best_score = sys.maxsize
    best_lower_bound = get_lower_bound(majority_matrix)
    matrix_size = len(majority_matrix)
    all_edges_as_stack = create_stack_from_matrix(majority_matrix)

    def rec(stack, r):
        nonlocal best_score
        nonlocal best_solution
        nonlocal best_lower_bound
        stack = add_transitive_closure_to_stack(stack, matrix_size)
        lower_bound_score = get_disagreements_for_stack(stack, majority_matrix)
        if lower_bound_score > best_lower_bound:
            return
        decomposition = tarjan_from_stack(subtract_stack_from_stack_undirected(all_edges_as_stack, stack))
        stack, unusable = create_stack_from_components(decomposition, stack, majority_matrix)

        possible_edges = all_edges_as_stack - stack - unusable
        if sum([len(x) for x in decomposition]) == len(decomposition):
            t = tarjan_from_stack(stack)
            vote = [x[0] for x in t]
            score = score_indexes_negative(graph_matrix, vote)
            if score < best_score:
                best_score = score
                best_solution = vote
                print(score)
                print(vote)
                print(stack)
                print('r', r)
                if lower_bound_score < best_lower_bound:
                    best_lower_bound = lower_bound_score
                    print('best_lower_bound', best_lower_bound)
        for e in sorted(list(possible_edges), key=lambda x: majority_matrix[x[0]][x[1]], reverse=True):
            if (e[1], e[0]) not in stack:
                rec(stack | {e}, r + 1)

    rec(set(), 0)
    return best_score, best_solution, [candidates_list[x] for x in best_solution]
