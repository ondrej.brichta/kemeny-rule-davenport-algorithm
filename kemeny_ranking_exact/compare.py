import os
import time
from multiprocessing import Process, Manager

from kemeny_final import solve, solve_bruteforce, generate_graph_matrix, kemeny_bab_with_stack, \
    kemeny_brute_force_negative, kemeny_not_davenport_bab


def kemeny_brute_force_negative_multi(candidates_matrix, candidates_list, return_list):
    start = time.time()
    return_list.extend(kemeny_brute_force_negative(candidates_matrix, candidates_list))
    return_list.append(round(time.time() - start, 4))


def kemeny_bab_with_stack_multi(candidates_matrix, candidates_list, return_list):
    start = time.time()
    return_list.extend(kemeny_bab_with_stack(candidates_matrix, candidates_list))
    return_list.append(round(time.time() - start, 4))


def kemeny_non_dav_with_stack_multi(candidates_matrix, candidates_list, return_list):
    start = time.time()
    return_list.extend(kemeny_not_davenport_bab(candidates_matrix, candidates_list))
    return_list.append(round(time.time() - start, 4))


def test_multiprocess():
    final = 'file, time_bab, time_brute, time_non_dav, same_core, score_bab, score_brute, score_non_dav'
    manager = Manager()
    for file in os.listdir("data/bruteforce_dataset/v2"):
        if file.endswith(".election"):
            print(file)
            candidates_matrix, candidates_list = generate_graph_matrix('data/bruteforce_dataset/v2/' + file)

            return_list_brute = manager.list()
            p1 = Process(target=kemeny_brute_force_negative_multi,
                         args=(candidates_matrix, candidates_list, return_list_brute))
            p1.start()
            p1.join(timeout=10*60)
            p1.kill()
            return_list_bab = manager.list()
            p2 = Process(target=kemeny_bab_with_stack_multi, args=(candidates_matrix, candidates_list, return_list_bab))
            p2.start()
            p2.join(timeout=10*60)
            p2.kill()
            return_list_non_dav = manager.list()
            p3 = Process(target=kemeny_non_dav_with_stack_multi,
                         args=(candidates_matrix, candidates_list, return_list_non_dav))
            p3.start()
            p3.join(timeout=10*60)
            p3.kill()
            if len(return_list_brute) == 0:
                return_list_brute.append(-1)
            if len(return_list_bab) == 0:
                return_list_bab.append(-1)
            if len(return_list_non_dav) == 0:
                return_list_non_dav.append(-1)

            final += '\n'
            final += f'{file}, {return_list_bab[-1]}, {return_list_brute[-1]},{return_list_non_dav[-1]}, ' \
                     f'{return_list_bab[0] == return_list_brute[0] and return_list_brute[0] == return_list_non_dav[0]}, ' \
                     f'{return_list_bab[0]}, {return_list_brute[0]}, {return_list_non_dav[0]}'
            print(final)
    text_file = open("Output_new.txt", "w")

    text_file.write(final)

    text_file.close()


if __name__ == '__main__':
    test_multiprocess()
